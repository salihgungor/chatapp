const shortid = require('shortid');
const redisClient = require('../redisClient');

function Rooms() {
	this.client = redisClient.getClient()
}

module.exports = new Rooms();

Rooms.prototype.upsert = function (name, recieverId='', senderId='') {
	const newId = shortid.generate();

	this.client.hset(
		'rooms',
		'@Room:'+ newId,
		JSON.stringify({
			id: '@Room:'+ newId,
			name,
			when: Date.now(),
			recieverId,
			senderId
		}),
		err => {
			if (err) {
			  console.error(err);
			}
		}
	)
};

Rooms.prototype.list = function (callback) {
	let roomList = [];

	this.client.hgetall('rooms', function (err, rooms) {
		if (err) {
			console.error(err);
			return callback([]);
		}

		for (let room in rooms){
			roomList.push(JSON.parse(rooms[room]));
		}

		return callback(roomList);
	})
};

Rooms.prototype.getByRoomName = function (roomName, callback) {
	let myRoom = {};
    this.client.hgetall('rooms', function (err, rooms) {
        if (err) {
            console.error(err);
            return callback([]);
        }

        for (let room in rooms){
        	let parseRoom = JSON.parse(rooms[room]);
        	if (roomName == parseRoom.name) {
                myRoom = JSON.parse(rooms[room]);
			}
        }
        return callback(myRoom);
    })
};

Rooms.prototype.getById = function (id, callback) {
    let myRoom = {};
    this.client.hgetall('rooms', function (err, rooms) {
        if (err) {
            console.error(err);
            return callback([]);
        }

        for (let room in rooms){
            let parseRoom = JSON.parse(rooms[room]);
            if (id == parseRoom.id) {
                myRoom = JSON.parse(rooms[room]);
            }
        }
        return callback(myRoom);
    })
};