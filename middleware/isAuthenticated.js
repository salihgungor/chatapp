function isAuthenticated(req, res, next){
	console.log('isAuthentication: ' + req.isAuthenticated());
	if (req.isAuthenticated())
		next();
	else
		res.redirect('/');
}

module.exports = isAuthenticated;
